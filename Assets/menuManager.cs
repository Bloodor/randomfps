﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuManager : MonoBehaviour
{
    public GameObject panel_tuto;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Play()
    {//Switch to the level scene
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void Quit()
    {//Quit the game
        Application.Quit();
    }

    public void HowtoPlay()
    {
        panel_tuto.SetActive(true);
    }

    public void Return()
    {
        panel_tuto.SetActive(false);
    }
}