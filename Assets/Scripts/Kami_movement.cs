﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

public class Kami_movement : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;

    public GameObject player;
    public float time;

    public float closeSpeed;
    public float awaySpeed;

    public float radius;

    //Makes the enemy target the player when he spawns
    void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //Makes the enemy move towards the player
        _navMeshAgent.destination = player.transform.position;
        
        if (Vector3.Distance(player.transform.position, transform.position) > 0.8f &
            Vector3.Distance(player.transform.position, transform.position) <= 3f)
        {
            _navMeshAgent.speed = closeSpeed;

        }
        else if (Vector3.Distance(player.transform.position, transform.position) > 3f)
            _navMeshAgent.speed = awaySpeed;
        else
        {
            Collider[] hitColliders = Physics.OverlapSphere(this.gameObject.transform.position, radius);
            foreach (var hitCollider in hitColliders)
            {
                if (hitCollider.gameObject.CompareTag("Enemy"))
                {
                    hitCollider.gameObject.GetComponent<Enemy_life>().health = 0;
                }

                if (hitCollider.gameObject.CompareTag("Player"))
                { 
                    HealthManger._instance.TakeDamage(4);
                }
            }
            Destroy(gameObject, 0);
            gameManager._instance.nb_enemy -=1;
        }
    }
}
//time += Time.deltaTime;
