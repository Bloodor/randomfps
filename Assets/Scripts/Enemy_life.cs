﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_life : MonoBehaviour
{
    public int health;
    public float sliderValue;
    
    public Slider slider;
    public GameObject healthBarUI;
    // Start is called before the first frame update
    void Start()
    {
        healthBarUI.SetActive(false);
        health = 20;
        slider.value = CalculateHealth();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            healthBarUI.SetActive(true);
            health -= 5;
            slider.value = CalculateHealth();

        }
    }

    private float CalculateHealth()
    {
        sliderValue = health / 20f;
        return sliderValue;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject, 0);
            gameManager._instance.nb_enemy -=1;
        }
            
    }
}
