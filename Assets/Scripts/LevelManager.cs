﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    //Switch scene accordingly to the button pressed
    public void Level1Choice()
    {
        
        gameManager.numberLvl = 1;
        SceneManager.LoadScene(2, LoadSceneMode.Single);
        
    }

    public void Level2Choice()
    {
        gameManager.numberLvl = 2;
        SceneManager.LoadScene(3, LoadSceneMode.Single);
        
    }

    public void Level3Choice()
    {
        gameManager.numberLvl = 3;
        SceneManager.LoadScene(4, LoadSceneMode.Single);
    }

    public void ReturnButton()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
