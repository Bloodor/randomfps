﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_headshot : MonoBehaviour
{
	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.CompareTag("Bullet"))
		{
			GetComponentInParent<Enemy_life>().health -= 10;
		}
	}
}
