﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
    private static gameManager _gameManager;
    public static gameManager _instance => _gameManager;
   
    public static int numberLvl = 3;
    public static int numberEnnemies;

    [Header("Settings Begin Game")] 
    public TextMeshProUGUI startGame_txt;
    public int playingLVL;
    public TextMeshProUGUI enemy_txt;
    public int nb_enemy;

    public  float waitingS = 1.5f;
    // Start is called before the first frame update
    private void Awake()
    {
        _gameManager = this;
        StartCoroutine(Tutorials());
        
        if (numberLvl == 1)
        { //Set up Lvl 1
            playingLVL = 1;
            numberEnnemies = 20;
            nb_enemy = numberEnnemies;
        }
        else if (numberLvl == 2)
        {
            playingLVL = 2;
            numberEnnemies = 10;
            nb_enemy = numberEnnemies;
        }
        else if (numberLvl == 3)
        {
            playingLVL = 3;
            numberEnnemies = 8;
            nb_enemy = numberEnnemies;
        }
       
    }

    void Start()
    {
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    IEnumerator Tutorials()
    {// Explain the objectives of the lvl
        Time.timeScale = 0.5f;
        yield return new  WaitForSeconds(2f);
        Time.timeScale = 1f;
        startGame_txt.gameObject.SetActive(false);
    }

    IEnumerator NextLevel()
    {    
        startGame_txt.gameObject.SetActive(true);
        startGame_txt.text = "Congratulations ! You win , Next level in " + waitingS;
        yield return new WaitForSeconds(waitingS);
        
    }
    // Update is called once per frame
    void Update()
    {
        // Update the enemy remaining
        enemy_txt.text =  nb_enemy.ToString();
        waitingS = Mathf.Clamp(waitingS, 0, Mathf.Infinity);
        // Foreach playingLevel i load the correpond scene
        if (nb_enemy <=0 && playingLVL == 1 || nb_enemy <= 0 && playingLVL == 2 || nb_enemy <=0 && playingLVL == 3)
        {
            waitingS -= Time.deltaTime;
            StartCoroutine(NextLevel());
            
            if (waitingS <= 0 && playingLVL == 1)
            {
                SceneManager.LoadScene(3, LoadSceneMode.Single);
            }else if (waitingS <= 0 && playingLVL == 2)
            {
                SceneManager.LoadScene(4, LoadSceneMode.Single);
            }
            else if (waitingS <= 0 && playingLVL == 3)
            {
                SceneManager.LoadScene(1, LoadSceneMode.Single);
            }
           
            
            Debug.Log("Level complete !");
            //Go to the next lvl
        }
    }
}
