﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class secretLvl1 : MonoBehaviour
{
    public TextMeshProUGUI txt_secret;
    public GameObject canvas_secret;
    public bool secret;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E) && secret )
        {
            StartCoroutine(Secret());
        }
    }

    IEnumerator Secret()
    {
        txt_secret.text = "Lol you unlock a secret!";
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
        Destroy(canvas_secret);
    }
   

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {//If the player enter the trigger he unlock the bool and can take the secret
            canvas_secret.SetActive(true);
            secret = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            canvas_secret.SetActive(false);
            secret = false;
        }
    }
}
