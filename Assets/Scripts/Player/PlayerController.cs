﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
      
    public float speed;
    public CharacterController controller;
    public float gravity = -9.6f;
    private Vector3 velocity;
    public static PlayerController _instance;
    private static PlayerController _playerController => _instance;
    // Start is called before the first frame update
    private void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // I get the float of the horizontal axis and vertical axis
        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");

        Vector3 move = transform.right * moveX + transform.forward * moveZ;
        // With the move method i put my vector 3 move then i use my float speed and Time.deltaTime to move
        controller.Move(move * speed * Time.deltaTime);

        //Simulate gravity
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }
}
