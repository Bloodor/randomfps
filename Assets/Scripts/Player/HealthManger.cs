﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Slider = UnityEngine.UI.Slider;

public class HealthManger : MonoBehaviour
{   
    public Slider ui_slider;
    
    public int maxhealth = 10;
    public int currenthealth;

    private static HealthManger _healthManager;

    public static HealthManger _instance => _healthManager;
    // Start is called before the first frame update
    private void Awake()
    {
        _healthManager = this;
    }

    void Start()
    {// Set up the slider and the health of player
        currenthealth = maxhealth;
        ui_slider.value = currenthealth;
        ui_slider.maxValue = maxhealth;
    }

    public void SetHealthBar()
    { //Update the slider
        ui_slider.value = currenthealth;
    }
    public void TakeDamage(int damage)
    {// The function for the player take damage
        currenthealth -=damage;
        SetHealthBar();
    }
    public void DieDebug()
    {// The function for the player dead
        Debug.Log("Player is Dead !!!");
        currenthealth = maxhealth;
        ui_slider.value = currenthealth;
    }
    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.G))
        {
            currenthealth = 1000000;
            SetHealthBar();
        }
        if (currenthealth <= 0)
        {
            
            StartCoroutine(Die());
        }
    }

    IEnumerator Die()
    {// The function for the player dead
        gameManager._instance.startGame_txt.gameObject.SetActive(true);
        PlayerController._instance.enabled = false;
        gameManager._instance.startGame_txt.color = Color.red;
        gameManager._instance.startGame_txt.text = "Your dead !";
        yield return new WaitForSeconds(2f);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
}
