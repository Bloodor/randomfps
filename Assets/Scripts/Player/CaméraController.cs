﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaméraController : MonoBehaviour
{
    public float mousesensitivity = 100f;

    public Transform playerbody;

    public float xRotation = 0f;
    // Start is called before the first frame update
    void Start()
    {    //Lock the cursor in play mode
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        //Get the mouse x axis and y axis
        float  mouseX = Input.GetAxis("Mouse X") * mousesensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mousesensitivity * Time.deltaTime;
        
        //Handle the rotation of the camera 
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);
        playerbody.Rotate(Vector3.up * mouseX);
        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
    }
}
