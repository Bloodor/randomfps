﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class players_life : MonoBehaviour
{
    public int life;
    // Start is called before the first frame update
    void Start()
    {
        life = 50;
    }

    // Update is called once per frame
    void Update()
    {
        if (life <= 0)
        {
            Destroy(gameObject, 0);
        }
    }
}
