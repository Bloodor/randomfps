﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class Z_movement : MonoBehaviour
{


    private NavMeshAgent _navMeshAgent;

    public GameObject player;
    public float time;

    public float close_speed;
    public float away_speed;

    //Makes the enemy target the player when he spawns
    void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        //Makes the enemy move towards the player
        _navMeshAgent.destination = player.transform.position;
        //if the enemy is in a certain range it slows & start hitting
        if (Vector3.Distance(player.transform.position, transform.position) > 0.3f & Vector3.Distance(player.transform.position, transform.position) <= 1.5f )
        {
            _navMeshAgent.speed = close_speed;
            if (time > 1.5f)
            {
                HealthManger._instance.TakeDamage(2);
                time = 0;
            }
        }
        //if the enemy is far from the player it runs towards him
        else if (Vector3.Distance(player.transform.position, transform.position) > 1.5f)
            _navMeshAgent.speed = away_speed;
        //if the enemy is in close range it stops & hits the player
        else
        {
            _navMeshAgent.speed = 0;
            if (time > 1.5f)
            {
                HealthManger._instance.TakeDamage(2);
                time = 0;
            }
        }
    }    
}