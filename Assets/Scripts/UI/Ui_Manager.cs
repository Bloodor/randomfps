﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Ui_Manager : MonoBehaviour
{
    public TextMeshProUGUI ammo_txt;
    public TextMeshProUGUI reloading_txt;
    private static Ui_Manager uiManager;
    public static Ui_Manager __instance => uiManager;

    public static bool gameisPaused = false;

    public GameObject pauseMenu;
    // Start is called before the first frame update
    public void Awake()
    {
        uiManager = this;
    }

    
    public void Setup_Ui_Ammo(TextMeshProUGUI textMeshProUgui, int nb_ammo)
    {
       
        textMeshProUgui.text = nb_ammo.ToString();
        
    }

    public void Resume()
    {//Resume the game
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        gameisPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void Pause()
    {//Pause the game and active the panel pause
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        gameisPaused = true;
    }

    public void LoadMenu()
    {//Return to the Menu Scene
        Time.timeScale = 1f;
        gameisPaused = false;
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void Quit()
    {
        Application.Quit();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameisPaused)
            {
                Resume();
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                Pause();
            }
        }
    }
}
