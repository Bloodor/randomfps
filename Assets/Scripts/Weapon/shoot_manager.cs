﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot_manager : MonoBehaviour
{
    [Header("Base datas")]
    public float fireRate = 0.4f;
    public GameObject bullet;
    public float bullet_speed;
    public float range = 100;
    public LayerMask layers;
    public int ammo;
    private int maxAmmo = 16;
    public bool reload;
    
    private Vector3 pos_cam;
    private Quaternion rot_cam;
    private Vector3 cam_shootpoint;

    [Header("Effect")] 
    public ParticleSystem muzzle_flash;
    public GameObject impact_effect;
    
    private static shoot_manager _pistolManager;
    public static shoot_manager __instance => _pistolManager;

    public Animator pistol_anim;
    // Start is called before the first frame update
    private void Awake()
    {
        _pistolManager = this;
    }

    void Start()
    {
        ammo = maxAmmo;
    }

   
    private void FixedUpdate()
    {
        //Update the pos and the rot of the camera so that the pistol's shoot going better
        pos_cam = Camera.main.transform.position;
        rot_cam = Camera.main.transform.rotation;
        cam_shootpoint = Camera.main.transform.forward;

    }

    // Update is called once per frame
    void Update()
    {
       
        fireRate -= Time.deltaTime;
        //Permet que le fireRate ne descende pas endessous de 0
        fireRate = Mathf.Clamp(fireRate, 0, Mathf.Infinity);
        //Call the shoot method
        if (Input.GetMouseButtonDown(0) && fireRate <= 0f && !reload && !Ui_Manager.gameisPaused)
        {
            Shoot();
            
        }
        

        if (Input.GetKeyDown(KeyCode.R) && ammo != 16)
        {
            reload = true;
            StartCoroutine(Reloading());
        }

        if (ammo <= 0 && !reload)
        {
            reload = true;
            Ui_Manager.__instance.reloading_txt.gameObject.SetActive(true);
            
        }


    }

    IEnumerator Reloading()
    {
        pistol_anim.Play("pistol-reload");
        yield return new WaitForSeconds(1f);
        ammo = maxAmmo;
        reload = false;
        Ui_Manager.__instance.reloading_txt.gameObject.SetActive(false);
        Ui_Manager.__instance.Setup_Ui_Ammo(Ui_Manager.__instance.ammo_txt, ammo);
        
    }
    void Shoot()
    {
        
        RaycastHit raycastHit;
        muzzle_flash.Play();
        ammo--;
        Ui_Manager.__instance.Setup_Ui_Ammo(Ui_Manager.__instance.ammo_txt, ammo);
        Debug.DrawRay(pos_cam, Camera.main.transform.forward * 100, Color.magenta, 3f);
        GameObject bullet = Instantiate(this.bullet, pos_cam, rot_cam);
        Physics.IgnoreCollision(bullet.GetComponent<SphereCollider>(),
            gameObject.GetComponentInParent<CharacterController>());
        bullet.GetComponent<Rigidbody>().AddForce(cam_shootpoint * bullet_speed, ForceMode.Impulse);
        pistol_anim.Play("pistol_shoot");
        
        
        fireRate = 0.4f;
        
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out raycastHit,range,layers  ))
        {
            Debug.Log(raycastHit.collider.name);
         
        }
    }
}
