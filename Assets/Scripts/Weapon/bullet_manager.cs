﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_manager : MonoBehaviour
{
    private SphereCollider _sphereCollider;
    // Start is called before the first frame update
    void Start()
    {
        _sphereCollider = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {

        Destroy(gameObject, 2f);
    }

    public void OnCollisionEnter(Collision other)
    { //Handle the collision between the bullet and the others objects and handle the impact effect
        if (other.gameObject.CompareTag("Wall") || other.gameObject.CompareTag("Enemy"))
        {
            GameObject impact = Instantiate(shoot_manager.__instance.impact_effect, gameObject.transform.position,
                Quaternion.LookRotation(gameObject.transform.position));
            Destroy(impact, 2);
            Destroy(gameObject);
            
        }

    }

}
