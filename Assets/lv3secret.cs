﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lv3secret : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Enemy"))
		{
			gameObject.GetComponent<BoxCollider>().isTrigger = false;
		}
	}
}
